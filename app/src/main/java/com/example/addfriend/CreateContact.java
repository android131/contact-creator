package com.example.addfriend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CreateContact extends AppCompatActivity implements View.OnClickListener {

    EditText etName, etNumber, etWebsite, etLocation;
    ImageView ivGreen, ivYellow, ivRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        etName = findViewById(R.id.etName);
        etNumber = findViewById(R.id.etNumber);
        etWebsite = findViewById(R.id.etWebsite);
        etLocation = findViewById(R.id.etLocation);
        ivGreen = findViewById(R.id.ivGreen);
        ivYellow = findViewById(R.id.ivYellow);
        ivRed = findViewById(R.id.ivRed);

        ivGreen.setOnClickListener(this);
        ivYellow.setOnClickListener(this);
        ivRed.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (etName.getText().toString().isEmpty() || etNumber.getText().toString().isEmpty()
                || etWebsite.getText().toString().isEmpty() || etLocation.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter all fields", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra("name", etName.getText().toString().trim());
            intent.putExtra("number", etNumber.getText().toString().trim());
            intent.putExtra("website", etWebsite.getText().toString().trim());
            intent.putExtra("location", etLocation.getText().toString().trim());

            if (v.getId() == R.id.ivGreen){
                intent.putExtra("mood", "green");
            } else if (v.getId() == R.id.ivYellow) {
                intent.putExtra("mood", "yellow");
            } else {
                intent.putExtra("mood", "red");
            }

            setResult(RESULT_OK, intent);
            CreateContact.this.finish();
        }
    }
}
